﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Airport.Core.Extensions;
using Airport.Core.Interfaces.Managers;
using Airport.Core.Interfaces.Repositories;
using Airport.Core.Models;
using MVC2.Models.Companies;


namespace MVC2.Controllers
{
    public class CompanyController : Controller
    {
        private readonly ICompanyManager _companyManager;
        //private readonly ICityManager _cityManager;
        private readonly ICityRepository _cityRepository;
        private readonly List<City> _cities;

        public CompanyController(ICompanyManager companyManager, ICityRepository cityRepository)
        {
            _companyManager = companyManager;
            _cityRepository = cityRepository;
            _cities = _cityRepository.SelectAll().ToList<City>();
        }

        // GET: Companies
        //[OutputCache(Duration = 300)] // 5 minutes
        public ActionResult Index()
        {
            ViewBag.IsAdmin = User.IsInRole("Admin");
            var companies = _companyManager.GetAllCompanies();
            return View(companies);
        }

        public ActionResult AddNewCompany()
        {
            var model = new CompanyModel
            {
                Cities = _cities
            };

            return View("CompanyDetails", model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddNewCompany(CompanyModel company)
        {
            ValidateCompanyName(company);
            if (!string.IsNullOrEmpty(company.CompanyName))
            {
                var record = _companyManager.GetAllCompanies().FirstOrDefault(x =>
                    string.Equals(x.CompanyName, company.CompanyName, StringComparison.InvariantCultureIgnoreCase));
                if (record != null) ModelState.AddModelError(nameof(company.CompanyName), $"We already have {company.CompanyName} in DB");
            }
            if (ModelState.IsValid)
            {
                _companyManager.AddCompany(company.CompanyName, company.SelectedCityId);
                return RedirectToAction("Index");
            }
            company.Cities = _cities;
            return View("CompanyDetails", company);
        }

        public ActionResult DeleteCompany(int companyId)
        {
            ViewBag.IsAdmin = User.IsInRole("Admin");
            _companyManager.DeleteCompany(companyId);
            var companies = _companyManager.GetAllCompanies();
            return View("Index", companies);
        }

        public ActionResult EditCompany(int companyId)
        {
            var model = GetModel(companyId);
            return View("CompanyDetails", model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult EditCompany(CompanyModel company)
        {
                ValidateCompanyName(company);
            if (ModelState.IsValidField("CompanyName"))
            {
                _companyManager.EditCompany(company.CompanyId, company.CompanyName, company.SelectedCityId);
                return RedirectToAction("Index");
            }
            company.Cities = _cityRepository.SelectAll().ToList<City>();
            return View("CompanyDetails",company);
        }

        public ActionResult CompanyPreview(int companyid)
        {
            var model = GetModel(companyid);
            return PartialView(model);
        }

        private CompanyModel GetModel(int companyId)
        {
            var company = _companyManager.GetCompanyById(companyId);
            var model = new CompanyModel
            {
                CompanyId = company.CompanyId,
                CompanyName = company.CompanyName,
                Cities = _cities,
                SelectedCityId = company.CityId,
                SelectedCityName = _cities.FirstOrDefault(x => x.CityId == company.CityId)?.CityName ?? ""
            };

            return model;
        }

        private void ValidateCompanyName(CompanyModel company)
        {
            company.CompanyName = company.CompanyName.Trim();
            //TODO(Done): get charArray and search char.IsLetter() , '-', ', ' '
            var nonLetter = false;
            var resolvedSymbols = new char[] { '-', '\'', ' ' };

            if (string.IsNullOrEmpty(company.CompanyName) || !char.IsLetter(company.CompanyName[0]))
            {
                ModelState.AddModelError("CompanyName", "The company name must begin with a letter");
                return;
            }
            foreach (var itm in company.CompanyName.ToCharArray())
            {
                if (!char.IsLetter(itm) && !resolvedSymbols.Contains(itm))
                {
                    nonLetter = true;
                    break;
                }
            }
            if (nonLetter)
            {
                ModelState.AddModelError("CompanyName", @"The company name can consist only of letters, whitespaces, signs - and '");
            }
            //TODO(Done): ToProperCase() ext method
            company.CompanyName = company.CompanyName.ToProperCase();          
        }
    }
}