﻿using System;
using System.Web.Mvc;

namespace MVC2.Controllers
{
    public class ErrorPageController : Controller
    {
        // http://benfoster.io/blog/aspnet-mvc-custom-error-pages
        // GET: Error
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Error(int statusCode, Exception exception)
        {
            Response.StatusCode = statusCode;
            ViewBag.StatusCode = statusCode + " Error";
            ViewBag.Exception = exception;

            return View();
        }
    }
}