﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Airport.Core.Enums;
using Airport.Core.Interfaces.Managers;
using Airport.Core.Interfaces.Repositories;
using Airport.Core.Models;
using MVC2.Models.Flights;

namespace MVC2.Controllers
{
    public class FlightController : Controller
    {
        private readonly IFlightManager _flightManager;
        private readonly ICompanyRepository _companyRepository;
        private readonly ICityRepository _cityRepository;
        private readonly ITerminalRepository _terminalRepository;
        private readonly List<Company> _companies;
        private readonly List<City> _cities;
        private readonly List<Terminal> _terminals;

        public FlightController(IFlightManager flightManager, ICompanyRepository companyRepository, ICityRepository cityRepository, ITerminalRepository terminalRepository)
        {
            _flightManager = flightManager;
            _companyRepository = companyRepository;
            _cityRepository = cityRepository;
            _terminalRepository = terminalRepository;
            _companies = _companyRepository.SelectAll().OrderBy(x => x.CompanyName).ToList();
            _cities = _cityRepository.SelectAll().OrderBy(x => x.CityName).ToList();
            _terminals = _terminalRepository.SelectAll().OrderBy(x => x.TerminalName).ToList();
        }
        // GET: Flight
        public ActionResult Index()
        {
            ViewBag.IsAdmin = User.IsInRole("Admin");
            var flights = _flightManager.GetAllFlights();
            return View(flights);
        }

        public ActionResult DeleteFlight(int flightId)
        {
            ViewBag.IsAdmin = User.IsInRole("Admin");
            _flightManager.DeleteFlight(flightId);
            var flights = _flightManager.GetAllFlights();
            return View("Index", flights);
        }

        public ActionResult EditFlight(int flightid)
        {
            var model = GetModel(flightid);
            return View("FlightDetails", model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult EditFlight(FlightModel flight)
        {
            ValidateFlight(flight);
            if (ModelState.IsValid)
            {
                var flightStatusEnum = (int)flight.FlightStatusId;
                //new DateTime(2018,02,20,20,40,00)
                // convert datetime2 to datetime

                var date = new DateTime(flight.FilghtDateTime.Year, flight.FilghtDateTime.Month, flight.FilghtDateTime.Day,
                    flight.FilghtDateTime.Hour, flight.FilghtDateTime.Minute, 0);

                _flightManager.EditFlight(flight.FlightId, flight.FlightNumber, date, flight.FlightDuration,
                    flightStatusEnum, flight.SelectedCityFrom, flight.SelectedCityTo, flight.SelectedCompanyId,
                    flight.SelectedTerminalId);
                return RedirectToAction("Index");
            }
            flight.Terminal = _terminals;
            flight.City = _cities;
            flight.Company = _companies;
            return View("FlightDetails", flight);
        }

        public ActionResult AddNewFlight()
        {
            var model = new FlightModel
            {
                Company = _companies,
                City = _cities,
                Terminal = _terminals,
            };
            return View("FlightDetails", model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddNewFlight(FlightModel flight)
        {
            ValidateFlight(flight);
            if (!string.IsNullOrEmpty(flight.FlightNumber))
            {
                var record = _flightManager.GetAllFlights().FirstOrDefault(x =>
                    string.Equals(x.FlightNumber, flight.FlightNumber, StringComparison.InvariantCultureIgnoreCase));
                if (record != null) ModelState.AddModelError(nameof(flight.FlightNumber), $"We already have {flight.FlightNumber} in DB");
            }
            if (ModelState.IsValid)
            {
                var flightStatusEnum = (int)flight.FlightStatusId;
                //new DateTime(2018,02,20,20,40,00)
                // convert datetime2 to datetime

                var date = new DateTime(flight.FilghtDateTime.Year, flight.FilghtDateTime.Month, flight.FilghtDateTime.Day,
                    flight.FilghtDateTime.Hour, flight.FilghtDateTime.Minute, 0);

                _flightManager.AddFlight(flight.FlightNumber, date, flight.FlightDuration,
                    flightStatusEnum, flight.SelectedCityFrom, flight.SelectedCityTo, flight.SelectedCompanyId,
                    flight.SelectedTerminalId);
                return RedirectToAction("Index");
            }
            flight.Terminal = _terminals;
            flight.City = _cities;
            flight.Company = _companies;
            return View("FlightDetails", flight);
        }

        private void ValidateFlight(FlightModel flight)
        {
            //FlightNumber validation
            flight.FlightNumber = flight.FlightNumber.Trim();
            flight.FlightNumber = flight.FlightNumber.Replace(" ", "");
            flight.FlightNumber = flight.FlightNumber.ToUpper();

            //DateTime validation
            if (flight.FilghtDateTime < DateTime.Now)
            {
                ModelState.AddModelError(nameof(flight.FilghtDateTime), "Selected date and time should not be posted yesterday or later");
            }

            //FlightDuration validation
            var hoursAndMinutes = flight.FlightDuration.Split(':');
            if (hoursAndMinutes[0].StartsWith("00"))
            {
                ModelState.AddModelError(nameof(flight.FlightDuration),
                    "Minimum flight time is not less than 1 hour");
            }
            if (hoursAndMinutes[0].Length == 1)
            {
                flight.FlightDuration = flight.FlightDuration.Insert(0,"0");
            }

            if (hoursAndMinutes[1].Length == 1)
            {
                flight.FlightDuration = flight.FlightDuration + "0";
            }

            var hours = int.Parse(hoursAndMinutes[0]);
            var minutes = int.Parse(hoursAndMinutes[1]);

            if (hours >= 22 & minutes > 40)
            {
                ModelState.AddModelError(nameof(flight.FlightDuration), "The longest at the moment non-stop flight is not more than 22 hours 40 minutes");
            }

            // Departure/Arrival point validation

            if (flight.SelectedCityFrom == flight.SelectedCityTo)
            {
                ModelState.AddModelError(nameof(flight.SelectedCityTo), "The cities of departure and arrival can not be the same");
            }



        }

        private FlightModel GetModel(int flightId)
        {
            var flight = _flightManager.GetFlightById(flightId);
            var model = new FlightModel
            {
                FlightId = flight.FlightId,
                Terminal = _terminals,
                City = _cities,
                Company = _companies,
                FilghtDateTime = flight.FilghtDateTime,
                FlightDuration = flight.FlightDuration,
                FlightNumber = flight.FlightNumber,
                FlightStatusId = (FlightStatusEnum)flight.FlightStatusId,
                SelectedCityFrom = flight.CityFromId,
                SelectedCityTo = flight.CityToId,
                SelectedTerminalId = flight.TerminalId,
                SelectedCompanyId = flight.CompanyId,
                //SelectedCityName = _cities.FirstOrDefault(x => x.CityId == company.CityId)?.CityName ?? ""
            };

            return model;
        }
    }
}