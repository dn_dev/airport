﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Airport.Core.Extensions;
using Airport.Core.Interfaces.Managers;
using Microsoft.Ajax.Utilities;
using MVC2.Models.Cities;

namespace MVC2.Controllers
{
    public class CityController : Controller
    {
        private readonly ICityManager _cityManager;

        public CityController(ICityManager cityManager)
        {
            _cityManager = cityManager;
        }

        // GET: Cities 
        public ActionResult Index()
        {
            ViewBag.IsAdmin = User.IsInRole("Admin");
            var cities = _cityManager.GetAllCities();
            return View(cities);
        }
        [HttpGet]
        public ActionResult AddNewCity()
        {
            return View("CityDetails",new CityModel());
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddNewCity(CityModel city)
        {
            ValidateCityName(city);

            if (ModelState.IsValid)
            {
                _cityManager.AddCity(city.CityName);
                return RedirectToAction("Index");
            }
            return View("CityDetails",city);
        }

        public ActionResult EditCity(int cityid)
        {
            var city = _cityManager.GetCityById(cityid);
            var model = new CityModel
            {
                CityId = city.CityId,
                CityName = city.CityName
            };

            return View("CityDetails", model);
        }

        public ActionResult DeleteCity(int cityId)
        {
            ViewBag.IsAdmin = User.IsInRole("Admin");
            _cityManager.DeleteCity(cityId);
            var cities = _cityManager.GetAllCities();
            return View("Index", cities);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult EditCity(CityModel city)
        {
            ValidateCityName(city);
            if (ModelState.IsValid)
            {
                _cityManager.EditCity(city.CityId,city.CityName);
                return RedirectToAction("Index");
            }
            return View("CityDetails", city);
        }

        private void ValidateCityName(CityModel city)
        {
            city.CityName = city.CityName.Trim();
            //TODO(Done): get charArray and search char.IsLetter() , '-', ', ' '
            var nonLetter = false;
            var resolvedSymbols = new []{ '-', '\'', ' ' };

            if (string.IsNullOrEmpty(city.CityName) || !char.IsLetter(city.CityName[0]))
            {
                ModelState.AddModelError("CityName", "The city name must begin with a letter");
                return;
            }
            foreach (var itm in city.CityName.ToCharArray())
            {
                if (!char.IsLetter(itm) && !resolvedSymbols.Contains(itm))
                {
                    nonLetter = true;
                    break;
                }
            }
            if (nonLetter)
            {
                ModelState.AddModelError("CityName", @"The city name can consist only of letters, whitespaces, signs - and '");
            }
            //TODO(Done): ToProperCase() ext method
            city.CityName = city.CityName.ToProperCase();
            // unique cityName
            if (!string.IsNullOrEmpty(city.CityName))
            {
                var record = _cityManager.GetAllCities().FirstOrDefault(x =>
                    string.Equals(x.CityName, city.CityName, StringComparison.InvariantCultureIgnoreCase));
                if (record != null) ModelState.AddModelError(nameof(city.CityName), $"We already have {city.CityName} in DB");
            }
        }
    }
}