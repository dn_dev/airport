﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Airport.Core.Extensions;
using Airport.Core.Interfaces.Managers;
using Airport.Core.Models;
using MVC2.Models.Terminals;

namespace MVC2.Controllers
{
    public class TerminalController : Controller
    {
        private readonly ITerminalManager _terminalManager;

        public TerminalController(ITerminalManager terminalManager)
        {
            _terminalManager = terminalManager;
        }
        // GET: Terminal
        public ActionResult Index()
        {
            ViewBag.IsAdmin = User.IsInRole("Admin");
            var terminals = _terminalManager.GetAllTerminals();
            return View(terminals);
        }

        [HttpGet]
        public ActionResult AddNewTerminal()
        {
            return View("TerminalDetails", new TerminalModel());
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddNewTerminal(TerminalModel terminal)
        {
            ValidateTerminalName(terminal);
            if (ModelState.IsValid)
            {
                _terminalManager.AddTerminal(terminal.TerminalName);
                return RedirectToAction("Index");
            }
            return View("TerminalDetails", terminal);
        }

        public ActionResult DeleteTerminal(int terminalId)
        {
            ViewBag.IsAdmin = User.IsInRole("Admin");
            _terminalManager.DeleteTerminal(terminalId);
            var terminals = _terminalManager.GetAllTerminals();
            return View("Index", terminals);
        }


        public ActionResult EditTerminal(int terminalId)
        {
            var terminal = _terminalManager.GetTerminalById(terminalId);
            var model = new TerminalModel
            {
                TerminalId = terminal.TerminalId,
                TerminalName = terminal.TerminalName
            };

            return View("TerminalDetails", model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult EditTerminal(TerminalModel terminal)
        {
            ValidateTerminalName(terminal);
            if (ModelState.IsValid)
            {
                _terminalManager.EditTerminal(terminal.TerminalId, terminal.TerminalName);
                return RedirectToAction("Index");
            }
            return View("TerminalDetails", terminal);
        }

        private void ValidateTerminalName(TerminalModel terminal)
        {
            terminal.TerminalName = terminal.TerminalName.Trim();
            //TODO(Done): get charArray and search char.IsLetter() , '-', ', ' '
            var nonLetter = false;
            var resolvedSymbols = new[] { '-', '\'', ' ' };

            if (string.IsNullOrEmpty(terminal.TerminalName) || !char.IsLetter(terminal.TerminalName[0]))
            {
                ModelState.AddModelError("TerminalName", "The terminal name must begin with a letter");
                return;
            }
            foreach (var itm in terminal.TerminalName.ToCharArray())
            {
                if (!char.IsLetter(itm) && !resolvedSymbols.Contains(itm))
                {
                    nonLetter = true;
                    break;
                }
            }
            if (nonLetter)
            {
                ModelState.AddModelError("TerminalName", @"The terminal name can consist only of letters, whitespaces, signs - and '");
            }
            //TODO(Done): ToProperCase() ext method
            terminal.TerminalName = terminal.TerminalName.ToProperCase();
            // unique cityName
            if (!string.IsNullOrEmpty(terminal.TerminalName))
            {
                var record = _terminalManager.GetAllTerminals().FirstOrDefault(x =>
                    string.Equals(x.TerminalName, terminal.TerminalName, StringComparison.InvariantCultureIgnoreCase));
                if (record != null) ModelState.AddModelError(nameof(terminal.TerminalName), $"We already have {terminal.TerminalName} in DB");
            }
        }
    }
}