﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Airport.Core.Extensions;
using Airport.Core.Interfaces.Managers;
using Airport.Core.Interfaces.Repositories;
using Airport.Core.Models;
using MVC2.Models.CompanyPersons;

namespace MVC2.Controllers
{
    public class CompanyPersonController : Controller
    {
        private readonly ICompanyPersonManager _companyPersonManager;
        private readonly ICompanyRepository _companyPersonRepository;
        private readonly List<Company> _companies;

        public CompanyPersonController(ICompanyPersonManager companyPersonManager, ICompanyRepository companyPersonRepository)
        {
            _companyPersonManager = companyPersonManager;
            _companyPersonRepository = companyPersonRepository;
            _companies = _companyPersonRepository.SelectAll().OrderBy(x => x.CompanyName).ToList();
        }

        // GET: CompanyPerson
        public ActionResult Index()
        {
            ViewBag.IsAdmin = User.IsInRole("Admin");
            var persons = _companyPersonManager.GetAllCompanyPersons();
            return View(persons);
        }

        public ActionResult AddNewCompanyPerson()
        {
            var model = new CompanyPersonModel()
            {
                Companies = _companies
            };
            return View("CompanyPersonDetails", model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddNewCompanyPerson(CompanyPersonModel person)
        {
            ValidateCompanyPerson(person);
            if (!string.IsNullOrEmpty(person.FirstName) && !string.IsNullOrEmpty(person.LastName) && person.SelectedCompanyId!=0)
            {
                var record = _companyPersonManager.GetAllCompanyPersons().FirstOrDefault(x => string.Equals(x.FirstName, person.FirstName,
                                                                                                    StringComparison.InvariantCultureIgnoreCase) &&
                                                                                                string.Equals(x.LastName, person.LastName,
                                                                                                    StringComparison.InvariantCultureIgnoreCase) &&
                                                                                                x.CompanyId == person.SelectedCompanyId);
                if (record != null) ModelState.AddModelError(nameof(person.LastName), $"We already have {person.FirstName} {person.LastName}" +
                                                                                      $" for selected company in DB");
            }
            //TODO: ValidatePersonName
            if (ModelState.IsValid)
            {
                _companyPersonManager.AddCompanyPerson(person.FirstName, person.LastName, person.Telephone,
                    person.Email, person.SelectedCompanyId);
                return RedirectToAction("Index");
            }
            person.Companies = _companies;
            return View("CompanyPersonDetails", person);
        }

        public ActionResult DeleteCompanyPerson(int presonId)
        {
            ViewBag.IsAdmin = User.IsInRole("Admin");
            _companyPersonManager.DeleteCompanyPerson(presonId);
            var presons = _companyPersonManager.GetAllCompanyPersons();
            return View("Index", presons);
        }

        public ActionResult EditCompanyPerson(int personId)
        {
            var model = GetModel(personId);
            return View("CompanyPersonDetails", model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult EditCompanyPerson(CompanyPersonModel person)
        {
            ValidateCompanyPerson(person);
            if (ModelState.IsValid)
            {
                _companyPersonManager.EditCompanyPerson(person.PersonId, person.FirstName, person.LastName, person.Telephone, person.Email, person.SelectedCompanyId);
                return RedirectToAction("Index");
            }
            person.Companies = _companyPersonRepository.SelectAll().ToList<Company>();
            return View("CompanyPersonDetails", person);
        }

        public ActionResult CompanyPersonPreview(int personId)
        {
            var model = GetModel(personId);
            return PartialView(model);
        }

        private CompanyPersonModel GetModel(int personId)
        {
            var person = _companyPersonManager.GetCompanyPersonById(personId);
            var model = new CompanyPersonModel
            {
                PersonId = person.PersonId,
                FirstName = person.FirstName,
                LastName = person.LastName,
                Email = person.Email,
                Telephone = person.Telephone,
                Companies = _companies,
                SelectedCompanyId = person.CompanyId,
                SelectedCompanyName = _companies.FirstOrDefault(x => x.CompanyId == person.CompanyId)?.CompanyName ?? ""
            };
            return model;
        }

        private void ValidateCompanyPerson(CompanyPersonModel person)
        {
            person.FirstName = person.FirstName.Trim();
            //TODO(Done): get charArray and search char.IsLetter() , '-', ', ' '
            var nonLetter = false;
            var resolvedSymbols = new char[] { '-', '\'', ' ' };

            if (string.IsNullOrEmpty(person.FirstName) || !char.IsLetter(person.FirstName[0]))
            {
                ModelState.AddModelError("FirstName", "The first name must begin with a letter");
                return;
            }
            foreach (var itm in person.FirstName.ToCharArray())
            {
                if (!char.IsLetter(itm) && !resolvedSymbols.Contains(itm))
                {
                    nonLetter = true;
                    break;
                }
            }
            if (nonLetter)
            {
                ModelState.AddModelError("FirstName", @"The first name can consist only of letters, whitespaces, signs - and '");
            }
            //TODO(Done): ToProperCase() ext method
            person.FirstName = person.FirstName.ToProperCase();

            if (string.IsNullOrEmpty(person.LastName) || !char.IsLetter(person.LastName[0]))
            {
                ModelState.AddModelError("LastName", "The first name must begin with a letter");
                return;
            }
            foreach (var itm in person.LastName.ToCharArray())
            {
                if (!char.IsLetter(itm) && !resolvedSymbols.Contains(itm))
                {
                    nonLetter = true;
                    break;
                }
            }
            if (nonLetter)
            {
                ModelState.AddModelError("LastName", @"The first name can consist only of letters, whitespaces, signs - and '");
            }
            //TODO(Done): ToProperCase() ext method
            person.LastName = person.LastName.ToProperCase();
        }
    }
}