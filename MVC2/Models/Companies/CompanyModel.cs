﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Airport.Core.Models;

namespace MVC2.Models.Companies
{
    public class CompanyModel
    {
        public int CompanyId { get; set; }

        [MinLength(3, ErrorMessage = "The length must be more than 3")]
        [DisplayName("Company name")]
        [Required(ErrorMessage = "Company name is required")]
        public string CompanyName { get; set; }

        [DisplayName("City")]
        public List<City> Cities { get; set; }

        //public List<CompanyContact> CompanyContacts;

        public int SelectedCityId { get; set; }

        [DisplayName("City")]
        public string SelectedCityName { get; set; }

        //public List<string> GetContactsWithPhones()
        //{
        //    return CompanyContacts.Select(x => x.ContactName + " " + x.Telephone).ToList();
        //}
    }
}
