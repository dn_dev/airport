﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Airport.Core.Models;
using Airport.Core.Enums;

namespace MVC2.Models.Flights
{
    public class FlightModel
    {
        public int FlightId { get; set; }

        [RegularExpression(pattern: "(?<![A-Z])[A-Z]{2}\\d{3,4}(?!\\d)",ErrorMessage = "Flight number format is two letter and three or four digits without spaces")]
        [Required(ErrorMessage = "Field can't be empty")]
        [DisplayName("Flight Number")]
        public string FlightNumber { get; set; }

        //[DataType(DataType.DateTime,ErrorMessage = "Date and time must be in format dd/mm/yyyy hh:mm")]
        [Required(ErrorMessage = "Field can't be empty")]
        [DisplayName("Date and Time")]
        [DisplayFormat(DataFormatString = "{0:s}", ApplyFormatInEditMode = true)]
        public DateTime FilghtDateTime { get; set;}

        //[RegularExpression(pattern: "^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Flight duration must be in format HH:MM")]
        [RegularExpression(pattern: "^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$", ErrorMessage = "Flight duration must be in format HH:MM")]
        [Required(ErrorMessage = "Field can't be empty")]
        [DisplayName("Duration(hh:mm)")]
        public string FlightDuration { get; set; }

        [Required(ErrorMessage = "Select flight status")]
        [DisplayName("Flight Status")]
        public FlightStatusEnum FlightStatusId { get; set; }

        [Required]
        [DisplayName("From City")]
        public int SelectedCityFrom { get; set; }

        [Required]
        [DisplayName("To City")]
        public int SelectedCityTo { get; set; }

        [Required]
        [DisplayName("Terminal")]
        public int SelectedTerminalId { get; set; }

        [Required]
        [DisplayName("Company")]
        public int SelectedCompanyId { get; set; }

        //[DisplayName("City")]
        public List<City> City { get; set; }

        [DisplayName("Company")]
        public List<Company> Company { get; set; }

        [DisplayName("Terminal")]
        public List<Terminal> Terminal { get; set; }
    }
}