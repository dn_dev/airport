﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Airport.Core.Models;

namespace MVC2.Models.CompanyPersons
{
    public class CompanyPersonModel
    {
        public int PersonId { get; set; }

        [MinLength(2, ErrorMessage = "The length must be more than 2")]
        [DisplayName("First Name")]
        [Required(ErrorMessage = "Field can't be empty")]
        public string FirstName { get; set; }

        [MinLength(2, ErrorMessage = "The length must be more than 2")]
        [DisplayName("Last Name")]
        [Required(ErrorMessage = "Field can't be empty")]
        public string LastName { get; set; }

        [DisplayName("Phone")]
        [Phone]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Provided phone number not valid")]
        [Required(ErrorMessage = "Your must provide a Phone Number")]
        public string Telephone { get; set; }

        [Display(Name = "Email address")]
        [EmailAddress]
        [Required(ErrorMessage = "Field can't be empty")]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }

        public int SelectedCompanyId { get; set; }

        [DisplayName("Company")]
        public string SelectedCompanyName { get; set; }

        [DisplayName("Companies")]
        public List<Company> Companies { get; set; }
    }
}