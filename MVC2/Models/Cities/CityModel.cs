﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MVC2.Models.Cities
{
    public class CityModel
    {
        public int CityId { get; set; }

        [DisplayName("City")]
        [MinLength(2,ErrorMessage = "Сity name must be at least two letters")]
        //[RegularExpression("(\\w|'|-|\\d)+(\\s+)", ErrorMessage = "The city name must begin with a capital letter")]
        public string CityName { get; set; }

    }
}