﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MVC2.Models.Terminals
{
    public class TerminalModel
    {
        public int TerminalId { get; set; }

        [DisplayName("Terminal")]
        [MinLength(1, ErrorMessage = "Terminal name must be at least one letter")]
        //[RegularExpression("(\\w|'|-|\\d)+(\\s+)", ErrorMessage = "The city name must begin with a capital letter")]
        public string TerminalName { get; set; }

    }
}