﻿using System;
using System.Linq;
using Airport.Core.Interfaces.Repositories;
using Airport.Core.Interfaces.Managers;
using Airport.Core.Interfaces.UnitOfWork;
using Airport.Core.Models;

namespace Airport.Business.Managers
{
    public class CompanyPersonManager : ICompanyPersonManager
    {
        private readonly ICompanyPersonRepository _companyPersonRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CompanyPersonManager(IUnitOfWork unitOfWork, ICompanyPersonRepository companyPersonRepository)
        {
            _unitOfWork = unitOfWork;
            _companyPersonRepository = companyPersonRepository;
        }

        public CompanyPerson[] GetAllCompanyPersons()
        {
            return _companyPersonRepository.SelectAll().ToArray();
        }

        public CompanyPerson GetCompanyPersonById(int id)
        {
            return _companyPersonRepository.Find(id);
        }

        public void AddCompanyPerson(string firstName, string lastName, string telephone, string email, int companyId)
        {
            var companyPerson = new CompanyPerson()
            {
                CompanyId = companyId,
                Email = email,
                Telephone = telephone,
                LastName = lastName,
                FirstName = firstName,
            };
            _companyPersonRepository.Insert(companyPerson);
            _unitOfWork.Save();
        }

        public void EditCompanyPerson(int personId, string firstName, string lastName, string telephone, string email,
            int companyId)
        {
            var record = _companyPersonRepository.Get(personId);
            if (record != null)
            {
                record.FirstName = firstName;
                record.LastName = lastName;
                record.Telephone = telephone;
                record.Email = email;
                record.CompanyId = companyId;
                _unitOfWork.Save();
            }
        }

        public void DeleteCompanyPerson(int presonlId)
        {
            var record = _companyPersonRepository.Get(presonlId);
            if (record != null)
            {
                _companyPersonRepository.Delete(record);
                _unitOfWork.Save();
            }
        }
    }
}
