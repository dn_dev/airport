﻿using System.Collections.Generic;
using Airport.Core;
using Airport.Core.BusinnessModels;
using Airport.Core.Enums;
using Airport.Core.Interfaces;

namespace Airport.Business.Managers
{
    public class ExperiencedFlightManager : IFlightManager
    {
        public void AddFlightInMemoryStorage()
        {
            throw new System.NotImplementedException();
        }

        public void DeleteFlightByNumberInMemoryStorage(int flightNumber)
        {
            throw new System.NotImplementedException();
        }

        public void EditFlightInMemoryStorage(int flightNumber, FlightFieldsEnum changedParameter)
        {
            throw new System.NotImplementedException();
        }

        public IFlight[] SearchFlightByCityAndTime(IFlight[] flights)
        {
            throw new System.NotImplementedException();
        }

        public bool SearchFlightByNumberInMemoryStorage(int flightNumber)
        {
            throw new System.NotImplementedException();
        }

        public IFlight[] SearchFlightByParameter(int searchParameter, IFlight[] flights)
        {
            throw new System.NotImplementedException();
        }

        List<Arrival> IFlightManager.GetArrivals()
        {
            // goto DB and read it
            throw new System.NotImplementedException();
        }

        List<Departure> IFlightManager.GetDepartures()
        {
            // goto DB and read it
            throw new System.NotImplementedException();
        }
    }
}
