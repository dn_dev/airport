﻿using System;
using System.Collections.Generic;
using System.Linq;
using Airport.Core.Interfaces.Managers;
using Airport.Core.Interfaces.Repositories;
using Airport.Core.Interfaces.UnitOfWork;
using Airport.Core.Models;

namespace Airport.Business.Managers
{
    public class FlightManager : IFlightManager
    {
        private readonly IFlightRepository _flightRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICityRepository _cityRepository;


        public FlightManager(IUnitOfWork unitOfWork, IFlightRepository flightRepository, ICityRepository cityRepository)
        {
            _unitOfWork = unitOfWork;
            _flightRepository = flightRepository;
            _cityRepository = cityRepository;
        }

        public Flight[] GetAllFlights()
        {
            var result = _flightRepository.SelectAll().ToArray();
            //// get all distinct cityId from flights
            //var cities = _cityRepository.SelectAll();
            //// get city from DB to Dictionary
            //Dictionary<int, City> dict = new Dictionary<int, City>();
            //foreach (var city in cities)
            //{
            //    dict[city.CityId] = city;
            //}

            //// Initialize navigation property CityFrom, CityTo
            //foreach (var itm in result)
            //{
            //    itm.CityTo = dict.ContainsKey(itm.CityToId) ? dict[itm.CityToId] : new City() { CityName = "Unknown" };
            //    itm.CityFrom = dict.ContainsKey(itm.CityFromId) ? dict[itm.CityFromId] : new City() { CityName = "Unknown" };
            //}
            return result;
        }

        public Flight GetFlightById(int id)
        {
            var result = _flightRepository.Get(id);
            //result.CityFrom = _cityRepository.Get(result.CityFromId);
            //result.CityTo = _cityRepository.Get(result.CityToId);

            return result;

        }

        public void AddFlight(string flightNumber, DateTime filghtDateTime, string flightDuration, int flightStatusId, int cityIdFrom,
            int cityIdTo, int companyId, int terminalId)
        {
            var flight = new Flight()
            {
                FlightNumber = flightNumber,
                FilghtDateTime = filghtDateTime,
                FlightDuration = flightDuration,
                FlightStatusId = flightStatusId,
                CityFromId = cityIdFrom,
                CityToId = cityIdTo,
                CompanyId = companyId,
                TerminalId = terminalId
            };
            _flightRepository.Insert(flight);
            _unitOfWork.Save();
        }

        public void EditFlight(int flightId, string flightNumber, DateTime filghtDateTime, string flightDuration, int flightStatusId,
            int cityIdFrom, int cityIdTo, int companyId, int terminalId)
        {
            var record = _flightRepository.Get(flightId);
            if (record != null)
            {
                record.FlightNumber = flightNumber;
                record.FilghtDateTime = filghtDateTime;
                record.FlightDuration = flightDuration;
                record.FlightStatusId = flightStatusId;
                record.CityFromId = cityIdFrom;
                record.CityToId = cityIdTo;
                record.CompanyId = companyId;
                record.TerminalId = terminalId;
                _unitOfWork.Save();
            }
        }

        public void DeleteFlight(int flightlId)
        {
            var record = _flightRepository.Get(flightlId);
            if (record != null)
            {
                _flightRepository.Delete(record);
                _unitOfWork.Save();
            }
        }
    }
}
