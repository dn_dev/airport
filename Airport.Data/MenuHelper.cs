﻿using System;
using Airport.Core.Enums;

namespace Airport.Data
{
    public class MenuHelper
    {
        public static bool TryEnumParse(string input, Enum CoreEnumForParse)
        {

            bool parseResult = false;
            input = input.Replace(' ', '_');
            switch (CoreEnumForParse)
            {
                case CoreEnum.AirlineCompany:
                    AirlineCompanyEnum airlineCompany;
                    parseResult = Enum.TryParse(input, out airlineCompany);
                    break;
                case CoreEnum.FlightStatus:
                    FlightStatusEnum flightStatus;
                    parseResult = Enum.TryParse(input, out flightStatus);
                    break;
                case CoreEnum.FlightType:
                    FlightTypeEnum flightType;
                    parseResult = Enum.TryParse(input, out flightType);
                    break;
                case CoreEnum.TerminalGate:
                    TerminalGateEnum terminalGate;
                    if (Enum.TryParse(input, out terminalGate))
                    {
                        parseResult = true;
                    }
                    break;
                case CoreEnum.TerminalName:
                    TerminalNameEnum terminalName;
                    parseResult = Enum.TryParse(input, out terminalName);
                    break;
                default:
                    parseResult = false;
                    break;
            }
            return parseResult;
        }
        public static int GetFlightNumber()
        {
            Console.WriteLine("Please enter Flight Number\n");
            int flightNumber = 0;
            string inputValue = Console.ReadLine();
            int.TryParse(inputValue, out flightNumber);
            while (flightNumber == 0)
            {
                Console.WriteLine($"\n\t\t\t\t\tYou entered a wrong flight number : {inputValue}");
                Console.WriteLine("Please enter new flight number again\n");
                inputValue = Console.ReadLine();
                int.TryParse(inputValue, out flightNumber);
            }
            while (inputValue.Length < 4 && inputValue.Length > 5)
            {

                Console.WriteLine($"\n\t\t\t\t\tFlight number must be four-digit");
                Console.WriteLine("Please enter new flight number again\n");
                inputValue = Console.ReadLine();
                int.TryParse(inputValue, out flightNumber);
            }
            //var flightWithSameNumber = MemoryStorage.Flights.Find(x => x.Number.Equals(flightNumber));
            //if (flightWithSameNumber != null)
            //{

            //    Console.WriteLine($"\n\t\t\t\t\tFlight with number {flightNumber} is exists");
            //    Console.WriteLine("Please enter new flight number again\n");
            //    inputValue = Console.ReadLine();
            //    int.TryParse(inputValue, out flightNumber);
            //}
            return flightNumber;
        }
        public static string GetCityName()
        {
            Console.WriteLine("\n\nEnter City\n");
            string inputValue = Console.ReadLine();
            char[] charArray = inputValue.ToCharArray();
            foreach (var item in charArray)
            {
                if (Char.IsDigit(item))
                {
                    Console.WriteLine($"\n\t\t\t\t\tYou entered a wrong city : {inputValue}");
                    Console.WriteLine("Please enter city again\n");
                    GetCityName();
                }
            }
            if (Char.IsLower(charArray[0]))
            {
                Console.WriteLine($"\n\t\t\t\t\tERROR : Wrong Format : {inputValue}");
                Console.WriteLine("The first character must be in uppercase \n");
                Console.WriteLine("Please enter city again\n");
                GetCityName();
            }
            return inputValue;
        }
        public static TerminalGateEnum GetTerminalGate()
        {
            Console.WriteLine("Please enter Terminal Gate\n");
            Console.WriteLine("1. First Gate");
            Console.WriteLine("2. Second Gate");
            Console.WriteLine("3. Third Gate\n");
            string inputValue = Console.ReadLine();
            while (!MenuHelper.TryEnumParse(inputValue, CoreEnum.TerminalGate))
            {
                Console.WriteLine($"\n\t\t\t\t\tYou entered a wrong gate number : {inputValue}");
                Console.WriteLine("Please enter Gate Number");
                inputValue = Console.ReadLine();
            }
            return (TerminalGateEnum)Enum.Parse(typeof(TerminalGateEnum), inputValue);
        }
        public static TerminalNameEnum GetTerminalName()
        {
            Console.WriteLine("Please enter Terminal Name (A, B or C)\n");
            string inputValue = Console.ReadLine();
            while (!MenuHelper.TryEnumParse(inputValue, CoreEnum.TerminalName))
            {
                Console.WriteLine($"\n\t\t\t\t\tYou entered a wrong terminal name : {inputValue}\n");
                Console.WriteLine("Please enter terminal name (A, B or C)\n");
                inputValue = Console.ReadLine();
            }
            return (TerminalNameEnum)Enum.Parse(typeof(TerminalNameEnum), inputValue);

        }
        public static AirlineCompanyEnum GetCarrier()
        {
            Console.WriteLine("Please enter Air Company Name\n");
            Console.WriteLine("List of current carriers: \n");
            Console.WriteLine("Air Baltic, Turkish Airlines, Egypt Air, Air France, Dniproavia, Wind Rose, KLM, Austrian Airlines, Lufthansa, Azur Air Ukraine, Atlasjet Ukraine, Azerbaijan Airlines\n");
            string inputValue = Console.ReadLine().Replace(' ', '_');
            while (!MenuHelper.TryEnumParse(inputValue, CoreEnum.AirlineCompany))
            {
                Console.WriteLine($"\n\t\t\t\t\tYou entered a non-existent carrier : {inputValue}\n");
                Console.WriteLine("Please enter Air Company Name\n");
                Console.WriteLine("List of current carriers: \n");
                Console.WriteLine("Air Baltic, Turkish Airlines, Egypt Air, Air France, Dniproavia, Wind Rose, KLM, Austrian Airlines, Lufthansa, Azur Air Ukraine, Atlasjet Ukraine, Azerbaijan Airlines\n");
                inputValue = Console.ReadLine().Replace(' ', '_');
            }
            return (AirlineCompanyEnum)Enum.Parse(typeof(AirlineCompanyEnum), inputValue);
        }
        public static FlightStatusEnum GetFlightStatus()
        {
            Console.WriteLine("Please enter status of new flight\n");
            Console.WriteLine("List of available flights status: \n");
            Console.WriteLine("CheckIn, Gate Closed, Arrived, Departed, Unknown, Canceled, Excpected, Delayed\n");
            string inputValue = Console.ReadLine().Replace(' ', '_');
            while (!MenuHelper.TryEnumParse(inputValue, CoreEnum.FlightStatus))
            {
                Console.WriteLine($"\n\t\t\t\t\tYou entered a non-existent flight status : {inputValue}\n");
                Console.WriteLine("Please enter Air Company Name\n");
                Console.WriteLine("List of flights status: \n");
                Console.WriteLine("CheckIn, Gate Closed, Arrived, Departed, Unknown, Canceled, Excpected, Delayed\n");
                inputValue = Console.ReadLine().Replace(' ', '_');
            }
            return (FlightStatusEnum)Enum.Parse(typeof(FlightStatusEnum), inputValue);
        }
        public static DateTime GetFlightTime()
        {
            Console.WriteLine("\nEnter the time for the flight in the format yyyy-mm-dd hh:mm\n");
            string inputValue = Console.ReadLine();
            DateTime.TryParse(inputValue, out DateTime dateTime);
            while (dateTime == DateTime.MinValue)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\nERROR: Wrong Date Format\n");
                Console.ForegroundColor = ConsoleColor.Gray;
                GetFlightTime();
            }
            return dateTime;
        }
    }
}


