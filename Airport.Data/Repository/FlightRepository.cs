﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airport.Core.Interfaces.Repositories;
using Airport.Core.Interfaces.UnitOfWork;
using Airport.Core.Models;

namespace Airport.Data.Repository
{
    public class FlightRepository : RepositoryBase<Flight>, IFlightRepository
    {
        public FlightRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Flight Get(int id)
        {
            return this.SelectFilterIncluding(x => x.FlightId == id, x => x.Company).FirstOrDefault();
        }

        public Flight[] GetAll()
        {
            return this.SelectIncluding(x => x.Company, x => x.Terminal).ToArray();
        }
    }
}
