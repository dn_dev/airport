﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airport.Core.Interfaces.Repositories;
using Airport.Core.Interfaces.UnitOfWork;
using Airport.Core.Models;

namespace Airport.Data.Repository
{
    class CompanyPersonRepository : RepositoryBase<CompanyPerson>, ICompanyPersonRepository
    {
        public CompanyPersonRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public CompanyPerson Get(int id)
        {
            return this.Select(x => x.PersonId == id).FirstOrDefault();
        }
    }
}
