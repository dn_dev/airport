﻿using System.Data.Entity.ModelConfiguration;
using Airport.Core.Models;

namespace Airport.Data.Mapping
{
    public class CityMap : EntityTypeConfiguration<City>
    {
        public CityMap()
        {
            // Primary Key
            this.HasKey(t => t.CityId);

            // Properties


            // Table & Column Mappings
            this.ToTable("City");
            //this.Property(x => x.CompanyName).HasColumnName("c1");

            // Navigation properties

            this.HasMany(t => t.Companies);
        }
    }
}