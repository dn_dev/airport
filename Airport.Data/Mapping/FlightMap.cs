﻿using System.Data.Entity.ModelConfiguration;
using Airport.Core.Models;

namespace Airport.Data.Mapping
{
    public class FlightMap : EntityTypeConfiguration<Flight>
    {
        public FlightMap()
        {
            this.HasKey(t => t.FlightId);

            this.ToTable("Flight");

            this.HasRequired(t => t.Company);

            this.HasRequired(t => t.Terminal);

            //this.HasRequired(t => t.CityFrom).WithMany(t => t.Flights).HasForeignKey(t => t.CityFromId);
            this.HasRequired(t => t.CityFrom).WithMany().HasForeignKey(t => t.CityFromId);
            //this.HasRequired(t => t.CityTo).WithMany(t => t.Flights).HasForeignKey(t => t.CityToId);
            this.HasRequired(t => t.CityTo).WithMany().HasForeignKey(t => t.CityToId);
            //Ignore(t => t.CityFrom);

            //Ignore(t => t.CityTo);
        }
    }
}
