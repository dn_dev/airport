﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airport.Core.Models;

namespace Airport.Data.Mapping
{
    class TerminalMap : EntityTypeConfiguration<Terminal>
    {
        public TerminalMap()
        {
            // Primary Key
            this.HasKey(t => t.TerminalId);

            // Properties


            // Table & Column Mappings
            this.ToTable("Terminal");
            //this.Property(x => x.CompanyName).HasColumnName("c1");

            // Navigation properties
            this.HasMany(t => t.Flights);
        }
    }
}
