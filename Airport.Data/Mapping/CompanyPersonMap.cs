﻿using System.Data.Entity.ModelConfiguration;
using Airport.Core.Models;

namespace Airport.Data.Mapping
{
    class CompanyPersonMap : EntityTypeConfiguration<CompanyPerson>
    {
        public CompanyPersonMap()
        {
            this.HasKey(t => t.PersonId);

            this.ToTable("CompanyPerson");

            this.HasRequired(t => t.Company);
        }
    }
}
