﻿CREATE TABLE [dbo].[CompanyPerson] (
    [PersonId]  INT          IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (50) NULL,
    [LastName]  VARCHAR (50) NULL,
    [Telephone] VARCHAR (50) NULL,
    [Email]     VARCHAR (50) NULL,
    [CompanyId] INT          NULL,
    CONSTRAINT [PK_CompanyPerson_PersonId] PRIMARY KEY CLUSTERED ([PersonId] ASC),
    CONSTRAINT [FK_CompanyPerson_CompanyId] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId]) ON DELETE CASCADE ON UPDATE CASCADE
);

