﻿CREATE TABLE [dbo].[tmp_devart_Flight] (
    [FlightId]       INT          NOT NULL,
    [FlightNumber]   VARCHAR (50) NULL,
    [FilghtDateTime] DATETIME     NULL,
    [FlightDuration] VARCHAR (50) NULL,
    [FlightStatusId] INT          NULL,
    [CityIdFrom]     INT          NULL,
    [CityIdTo]       INT          NULL,
    [CompanyId]      INT          NULL,
    CONSTRAINT [tmp_devart_PK_Flight_FlightId] PRIMARY KEY CLUSTERED ([FlightId] ASC)
);

