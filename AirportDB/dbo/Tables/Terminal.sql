﻿CREATE TABLE [dbo].[Terminal] (
    [TerminalId]   INT          IDENTITY (1, 1) NOT NULL,
    [TerminalName] VARCHAR (50) NULL,
    CONSTRAINT [PK_Terminal_TerminalId] PRIMARY KEY CLUSTERED ([TerminalId] ASC)
);

