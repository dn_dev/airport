﻿
namespace Airport.Core.FlightEventArgs
{
    public class FlightEventArgs
    {
        public int FlightCountOld { get; set; }
        public int FlightCountNew { get; set; }
    }
}