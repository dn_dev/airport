﻿using System;
using Airport.Core.Enums;
using Airport.Core.Interfaces;

namespace Airport.Core.BusinnessModels
{
    public class Departure: IFlight
    {
        public FlightTypeEnum FlightType { get; set; }
        public FlightStatusEnum FlightStatus { get; set; }
        public TerminalNameEnum TerminalName { get; set; }
        public AirlineCompanyEnum AirlineCompany { get; set; }
        public string City { get; set; }
        public TerminalGateEnum TerminalGate { get; set; }
        public int Number { get; set; }
        public DateTime Time { get; set; }
    }
}
