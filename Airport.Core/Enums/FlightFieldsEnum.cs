﻿namespace Airport.Core.Enums
{
    public enum FlightFieldsEnum
    {
        Time = 1,
        Number = 2,//
        AirlineCompany = 3,//
        City = 4,
        TerminalName = 5,//
        TerminalGate = 6,//
        FlightStatus = 7,//
    }
}
