﻿namespace Airport.Core.Enums
{
    public enum FlightStatusEnum
    {
        Check_In,
        Gate_Closed,
        Arrived,
        Departed,
        Unknown,
        Canceled,
        Excpected,
        Delayed,
    }
}
