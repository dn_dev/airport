﻿namespace Airport.Core.Enums
{
    public enum CoreEnum
    {
        AirlineCompany,
        FlightStatus,
        FlightType,
        TerminalGate,
        TerminalName
    }
}
