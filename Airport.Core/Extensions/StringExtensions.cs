﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airport.Core.Extensions
{
    public static class StringExtensions
    {
        public static string ToProperCase(this string text)
        {
            var result = string.Empty;
            if(!string.IsNullOrWhiteSpace(text)) result = text.Replace(text[0], char.ToUpper(text[0]));
            return result;
        }
    }
}
