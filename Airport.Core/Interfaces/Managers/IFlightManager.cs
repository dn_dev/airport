﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airport.Core.Models;

namespace Airport.Core.Interfaces.Managers
{
    public interface IFlightManager
    {
        Flight[] GetAllFlights();

        Flight GetFlightById(int id);

        void AddFlight(string flightNumber, DateTime filghtDateTime, string flightDuration, int flightStatusId, int cityIdFrom, int cityIdTo, int companyId, int terminalId);

        void EditFlight(int flightId, string flightNumber, DateTime filghtDateTime, string flightDuration, int flightStatusId, int cityIdFrom, int cityIdTo, int companyId, int terminalId);

        void DeleteFlight(int flightId);
    }
}
