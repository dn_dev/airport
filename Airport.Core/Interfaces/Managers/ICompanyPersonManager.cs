﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airport.Core.Models;

namespace Airport.Core.Interfaces.Managers
{
    public interface ICompanyPersonManager
    {
        CompanyPerson[] GetAllCompanyPersons();

        CompanyPerson GetCompanyPersonById(int id);

        void AddCompanyPerson(string firstName, string lastName, string telephone, string email, int companyId);

        void EditCompanyPerson(int companyPersonId, string firstName, string lastName, string telephone, string email,
            int companyId);

        void DeleteCompanyPerson(int id);
    }
}
