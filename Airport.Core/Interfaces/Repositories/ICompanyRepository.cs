﻿using Airport.Core.Models;

namespace Airport.Core.Interfaces.Repositories
{
    public interface ICompanyRepository : IRepositoryBase<Company>
    {
        Company Get(int id);
    }
}
