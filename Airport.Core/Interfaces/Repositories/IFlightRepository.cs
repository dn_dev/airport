﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airport.Core.Models;

namespace Airport.Core.Interfaces.Repositories
{
    public interface IFlightRepository : IRepositoryBase<Flight>
    {
        Flight Get(int id);
        Flight[] GetAll();
    }
}
