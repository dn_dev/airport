﻿using Airport.Core.Models;

namespace Airport.Core.Interfaces.Repositories
{
    public interface ICompanyPersonRepository : IRepositoryBase<CompanyPerson>
    {
        CompanyPerson Get(int id);
    }
}
