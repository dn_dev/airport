﻿namespace Airport.Core.Structures
{
    public struct TableStructure
    {
        public string[] Header { get; set; }
        public TableRow[] Rows { get; set; }
    }

    public struct TableRow
    {
        public string[] Cells { get; set; }
    }
}
