﻿using System.Collections.Generic;
using System.Diagnostics;

namespace Airport.Core.Models
{
    public class City
    {

        public int CityId { get; set; }

        public string CityName { get; set; }

        public virtual ICollection<Company> Companies { get; set; }

    }

}