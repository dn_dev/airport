﻿using System;
using Airport.Core.Enums;

namespace Airport.Core
{
    public interface IFlight
    {
        FlightTypeEnum FlightType { get; set; }             // Uses Airport.Data.Enums
        FlightStatusEnum FlightStatus { get; set; }         // Uses Airport.Data.Enums
        TerminalNameEnum TerminalName { get; set; }         // Uses Airport.Data.Enums
        AirlineCompanyEnum AirlineCompany { get; set; }     // Uses Airport.Data.Enums
        string City { get; set; }                           // Input
        TerminalGateEnum TerminalGate { get; set; }         // Uses Airport.Data.Enums
        int Number { get; set; }                            // Uses Airport.Data.Enums
        DateTime Time { get; set; }                         // Input
    }
}