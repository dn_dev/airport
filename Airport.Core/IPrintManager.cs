﻿using System.Collections.Generic;

namespace Airport.Core
{
    public interface IPrintManager
    {
        void Print(IList<IFlight> flights);
        void Print();
        void PrintMenu();
        void PrintSubmenu(int number);
    }
}
